import numpy as np
from TASK_1.VisualizeNN import DrawNN
from TASK_1.utils import save_weights, load_weights
from TASK_1.AcrivationFunction import get_functions


class NeuralNetwork:

    def __init__(self, nodes_in_layers, learning_rate, activation_function, initial_weights_path=None):
        self.nodes_in_layers = nodes_in_layers

        self.learning_rate = learning_rate
        self.activation_function, self.activation_derivative = get_functions(activation_function)

        if not initial_weights_path:
            self.weights = self.init_weights()
        else:
            self.weights = load_weights(initial_weights_path)

        # print(f'initialized new NN with random weights: {self.weights}')

    def init_weights(self):
        tmp_weights = []
        weights = []
        for nodes_in_layer in self.nodes_in_layers:
            tmp_weights.append(np.random.uniform(-1.0, 0.0, nodes_in_layer)[np.newaxis])

        for idx, layer_weight in enumerate(tmp_weights[:-1]):
            weights.append(layer_weight.T * tmp_weights[idx + 1])

        return weights

    def save_weights(self, path):
        save_weights(path, self.weights)

    def update_weights(self, expected, outputs):
        current_error = expected - outputs[-1]
        current_delta = current_error * self.activation_derivative(outputs[-1])

        a = outputs[-2][np.newaxis].T
        self.weights[-1] += self.learning_rate * a.dot(current_delta[np.newaxis])

        for i in range(len(self.weights) - 1, 0, -1):
            current_error = current_delta.dot(self.weights[i].T)
            current_delta = current_error * self.activation_derivative(outputs[i])

            a = outputs[i - 1][np.newaxis].T
            c = self.learning_rate * a.dot(current_delta[np.newaxis])
            self.weights[i - 1] += c

    def run(self, inputs):
        outputs = [inputs]
        for btwin_layer in self.weights:
            outputs.append(self.activation_function(outputs[-1].dot(btwin_layer)))

        return outputs

    def train(self, data, iterations):
        for iter in range(iterations):
            for inputs, expected in data:
                outputs = self.run(inputs)
                self.update_weights(expected, outputs)
            if iter % 1000 == 0:
                print(f'iteration no: {iter}')
                # print(self.weights)
                DrawNN(self.nodes_in_layers, self.weights).draw()

    def check(self, inputs):
        outputs = self.run(inputs)

        # remove inputs
        return outputs[-1]
