import pickle


def save_weights(path, weights):
    with open(path, mode='wb') as file:
        pickle.dump(weights, file)


def load_weights(path):
    with open(path, mode='rb') as file:
        return pickle.load(file)