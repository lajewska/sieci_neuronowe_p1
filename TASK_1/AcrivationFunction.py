from numpy import exp, maximum


def sigmoid(x):
    return 1 / (1 + exp(-x))


# The derivative of the Sigmoid function.
# This is the gradient of the Sigmoid curve.
# It indicates how confident we are about the existing weight.
def sigmoid_derivative(x):
    return x * (1 - x)


def relu(x):
    return maximum(x, 0.0)


def relu_derivative(x):
    return (x < 2) + 0.0


def get_functions(function_name):
    if function_name == 'relu':
        return relu, relu_derivative

    return sigmoid, sigmoid_derivative
