from TASK_1.NeuralNetwork import NeuralNetwork
from TASK_1.AcrivationFunction import sigmoid
import numpy as np
import csv


def read_csv_data(path):
    data = []

    with open(path) as csvDataFile:
        reader = csv.reader(csvDataFile)
        next(reader, None)  # skip 1st line
        for row in reader:
            input_data, class_data = list(map(float, row[:-1])), float(row[-1])
            class_data = np.array([1.0, 0.0] if class_data == 1.0 else [0.0, 1.0])
            input_data = np.array(input_data)
            data.append([input_data, class_data])

    return data


def main():
    np.random.seed(13)

    data = read_csv_data('../projekt1_data/classification/data.simple.test.100.csv')
    # nn1 = NeuralNetwork([2, 4, 2], learning_rate=0.1, activation_function=sigmoid, initial_weights_path='weights1.csv')

    nn1 = NeuralNetwork([2, 4, 2], learning_rate=0.1, activation_function='sigmoid') # or relu

    nn1.train(data, 100)
    # nn1.save_weights('weights1.csv')

    print("from train dataset: ")
    print(f'nn answered: {nn1.check(data[0][0])}, should be: {data[0][1]}')
    print(f'nn answered: {nn1.check(data[39][0])}, should be: {data[39][1]}')

if __name__ == '__main__':
    main()
