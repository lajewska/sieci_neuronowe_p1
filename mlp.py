from numpy import exp, array, random, dot
import csv


class NeuronLayer():
    def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
        self.synaptic_weights = 2 * random.random((number_of_inputs_per_neuron, number_of_neurons)) - 1


class NeuralNetwork():
    def __init__(self, layers):
        self.layers = layers
        self.number_of_layers = len(layers)

    # The Sigmoid function, which describes an S shaped curve.
    # We pass the weighted sum of the inputs through this function to
    # normalise them between 0 and 1.
    def __sigmoid(self, x):
        return 1 / (1 + exp(-x))

    # The derivative of the Sigmoid function.
    # This is the gradient of the Sigmoid curve.
    # It indicates how confident we are about the existing weight.
    def __sigmoid_derivative(self, x):
        return x * (1 - x)

    # We train the neural network through a process of trial and error.
    # Adjusting the synaptic weights each time.
    def train(self, training_set_inputs, training_set_outputs, number_of_training_iterations):
        for iteration in range(number_of_training_iterations):
            # Pass the training set through our neural network
            outputs = self.think(training_set_inputs)

            # Calculate the error for last layer (The difference between the desired output
            # and the predicted output).
            last_layer_index = self.number_of_layers - 1
            current_error = training_set_outputs - outputs[last_layer_index]
            current_delta = current_error * self.__sigmoid_derivative(outputs[last_layer_index])

            layers_adjustments = []

            # Calculate how much to adjust the weights by
            current_layer_adjustment = outputs[last_layer_index - 1].T.dot(current_delta)
            layers_adjustments.append(current_layer_adjustment)

            # Calculate the errors for the rest of the layers (By looking at the weights in layer 1,
            # we can determine by how much layer 1 contributed to the error in layer 2).
            for i in range(1, self.number_of_layers):
                layer_index = self.number_of_layers - i - 1
                current_error = current_delta.dot(self.layers[layer_index + 1].synaptic_weights.T)
                current_delta = current_error * self.__sigmoid_derivative(outputs[layer_index])
                # Calculate how much to adjust the weights by
                if layer_index == 0:
                    current_layer_adjustment = training_set_inputs.T.dot(current_delta)
                else:
                    current_layer_adjustment = outputs[layer_index - 1].T.dot(current_delta)
                layers_adjustments.append(current_layer_adjustment)

            # Adjust the weights.
            for i in range(self.number_of_layers):
                self.layers[i].synaptic_weights += layers_adjustments[self.number_of_layers - i - 1]


    # The neural network thinks.
    def think(self, inputs):
        current_input = inputs
        outputs = []
        for i in range(0, self.number_of_layers):
            current_input = self.__sigmoid(dot(current_input, self.layers[i].synaptic_weights))
            outputs.append(current_input)
        return outputs


    # The neural network prints its weights
    def print_weights(self):
        for i in range(0, self.number_of_layers):
            print("    Layer " + str(i + 1) + ":")
            print(self.layers[i].synaptic_weights)


def arrayFromCSVFile(path, numOfArgs):
    inputs = []
    outputs = []

    with open(path) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if row[0] != 'x':
                rowInput = []
                for i in range(0, numOfArgs-1):
                    rowInput.append(float(row[i]))
                inputs.append(rowInput)
                rowOutput = []
                rowOutput.append(int(row[numOfArgs-1]))
                outputs.append(rowOutput)

    return array(inputs), array(outputs)


if __name__ == "__main__":

    # Seed the random number generator
    random.seed(1)

    layers = []
    layers.append(NeuronLayer(3, 2))
    layers.append(NeuronLayer(2, 3))
    layers.append(NeuronLayer(1, 2))

    # Combine the layers to create a neural network
    neural_network = NeuralNetwork(layers)

    print("Stage 1) Random starting synaptic weights: ")
    neural_network.print_weights()

    # The training set. We have 7 examples, each consisting of 3 input values
    # and 1 output value.
    training_set_inputs, training_set_outputs = arrayFromCSVFile('C:\\Users\\werka\\Desktop\\pw\\Sieci neuronowe\\Dane\\projekt1\\classification\\data.simple.test.100.csv', 3)

    # print(training_set_inputs)
    # print(training_set_outputs)

    # Train the neural network using the training set.
    # Do it 60,000 times and make small adjustments each time.
    neural_network.train(training_set_inputs, training_set_outputs, 60000)

    print("Stage 2) New synaptic weights after training: ")
    neural_network.print_weights()

    # Test the neural network with a new situation.
    print("Stage 3) Considering a new situation [0.316172221675515, -0.652028118725866] -> ?: ")
    output = neural_network.think(array([-0.00292545510455966, 0.722530109807849]))
    print(output)